<?php

session_start();

require 'config.php';

require 'classes/messages.php';
require 'classes/urlhandler.php';
require 'classes/controller.php';
require 'classes/model.php';

require 'controllers/home.php';
require 'controllers/books.php';

require 'models/home.php';
require 'models/book.php';

$urlhandler = new UrlHandler($_GET);
$controller = $urlhandler->createController();

if ($controller) {
	$controller->executeAction();
}