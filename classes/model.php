<?php

abstract class Model {
	protected $pdo;
	protected $stmt;
	protected $message;

	public function __construct() {
		$dsn = "pgsql:host=" . DB_HOST . ";port=5432;dbname=" . DB_NAME;
		$opt = array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES => false,
		);
		$this->pdo = new PDO($dsn, DB_USER, DB_PASS, $opt);
	}

	public function query($query) {
		$this->stmt = $this->pdo->prepare($query);
	}

	public function bind($param, $value, $type = NULL) {
		if (is_null($type)) {
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;

				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
					
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;

				default:
					$type = PDO::PARAM_STR;
			}
		}
		$this->stmt->bindValue($param, $value, $type);
	}

	public function execute() {
		$this->stmt->execute();
	}

	public function fetch() {
		$this->execute();
		return $this->stmt->fetchAll();
	}

	public function fetchOne() {
		$this->execute();
		return $this->stmt->fetch();
	}

	public function lastInsertId() {
		return $this->pdo->lastInsertId();
	}

}