<?php

class Books extends Controller {

	protected function index() {
		$viewmodel = new BookModel();
		$this->returnView($viewmodel->index(), true);
	}

	protected function insert() {
		$viewmodel = new BookModel();
		$this->returnView($viewmodel->insert(), true);
	}

	protected function search() {
		$viewmodel = new BookModel();
		$this->returnView($viewmodel->search(), true);
	}


}