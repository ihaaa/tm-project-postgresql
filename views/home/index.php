<div class="text-center">
	<h1>Project Summary</h1>
	<hr />
</div>
<div>
	<p>
		<strong>Architecture:</strong> I structured the application as per the MVC pattern and OOP way of programming. We have a "UrlHandler" class which creates (instantiates) the MVC controller based on the URL - I used an .htaccess file and $_GET to achieve this. The controller instantiates the "Modal" class and displays the relevant "views". The "Modal" class manages the provided data, it contains our database handler, queries and overall logic of the application. The "views" are the output, there is a "main"-view in which the controller loads a specific view for the relevant class/method. We have an additional class "Messages" which I used to manage the output of success/error messages. The methods in the "Messages" class are static and I used the "$_SESSION" superglobal when displaying the appropriate error messages.
	</p>
	<p>
		<strong>HTML:</strong> Everything is pretty standard here, we have a navigation and a couple of forms.<br />
		<strong>CSS:</strong> I used bootstrap and added a scrolling banner I saw on <a href="https://www.reddit.com/r/Art/" target="_blank">reddit</a> some time ago.<br />
		<strong>JS:</strong> Just bootstrap.
	</p>
	<p>
		<strong>Description:</strong>
		<ol>
			<li>We have a form in which we can enter and submit the directory of a folder tree. Afterwards an array with the directories of all the XML files is created (I used the recursive iterator). We check for duplicates. If there is a record of the same element in our database we update the date of the record to the current date. If the record is not duplicated we insert it into our database and insert all its details into an array. After we have cycled trough all XML files (I used simplexml_load_file()) we display the array which contains all the records that have been entered into the database.</li>
			<li>I used UTF-8 encoding so that Cyrillic, Korean and Japanese characters are displayed properly.</li>
			<li>The search page has a dropdown list with the authors in the database.</li>
		</ol>
	</p>

</div>