<!-- <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Search by Author Text</h3>
  </div>
  <div class="panel-body">
    
  	<form name="searchText" method="post" action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>">
			<div class="form-group">		
	  		<label for="author">Author:</label>
        <input class="form-control" id="author" type="text" name="author" />
			</div>
			<input class="btn btn-primary" type="submit" name="submit" value="Search" />
  	</form>

  </div>
</div> -->

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Search by Author</h3>
  </div>
  <div class="panel-body">
    
    <form name="searchDrop" method="post" action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>">
      <div class="form-group">    
        <label for="author">Author:</label>
        <select class="form-control" id="author" name="author">
          
          <?php $options = new BookModel(); $options->dropdown(); ?>

        </select>
      </div>
      <input class="btn btn-primary" type="submit" name="submit" value="Search" />
    </form>

  </div>
</div>

<div>

  <?php
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $viewmodel != NULL) { 
    foreach($viewmodel as $item) { 
  ?>

    <div class="well">
      
      <p>Author: <?php echo $item['author']; ?></p>
      <p>Book: <?php echo $item['book_name']; ?></p>
      <p>Record Date: <?php echo $item['record_date']; ?></p>
      <p>Record Location: <?php echo $item['xml_path']; ?></p>

    </div>

  <?php }} ?>

</div>