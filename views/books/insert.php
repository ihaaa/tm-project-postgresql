<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Insert Book Records</h3>
  </div>
  <div class="panel-body">
    
  	<form name="addBook" method="post" action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>">
			<div class="form-group">		
	  		<label for="path">Enter the relative path to the XML tree folder directory:</label>
	  		<input id="path" class="form-control" type="text" name="path" placeholder='Example: "xml" OR "xml\books"' />
			</div>
			<input class="btn btn-primary" type="submit" name="submit" value="Select" />
  	</form>

  </div>
</div>

<div>

  <?php
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $viewmodel != NULL) { 
    foreach($viewmodel as $item) { 
  ?>

    <div class="well">
      
      <p>Author: <?php echo $item['author']; ?></p>
      <p>Book: <?php echo $item['book_name']; ?></p>
      <p>Record Date: <?php echo $item['record_date']; ?></p>
      <p>Record Location: <?php echo $item['xml_path']; ?></p>

    </div>

  <?php }} ?>

</div>