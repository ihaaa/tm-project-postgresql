<?php

class BookModel extends Model {

	public function index() {
		$this->query('SELECT * FROM books');
		$rows = $this->fetch();
		return $rows;
	}

	public function insert() {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {

			if (empty($_POST['path'])) {
				Messages::setMessage('Please enter a directory', 'error');
				return;
			}

			$path = $_POST['path'];
			$dirs = array();

			if(!file_exists($path)) {
			  Messages::setMessage('Please enter a valid directory', 'error');
			} else {
			  $directory = new RecursiveDirectoryIterator($path);
				$Iterator = new RecursiveIteratorIterator($directory);
				foreach (new RegexIterator($Iterator, '/^.+\.xml$/i', RecursiveRegexIterator::GET_MATCH) as $filename => $file) {
				  array_push($dirs, $filename);
				}
			}

			$rows = array();

			foreach ($dirs as $xmlPath) {
				$xml = simplexml_load_file($xmlPath) or die("Error: Cannot create object");

			  foreach ($xml->children() as $row) {
			    $author = $row->author;
			    $name = $row->name;

			    $this->query('SELECT * FROM books WHERE xml_path = :xml_path AND author = :author AND book_name = :book_name');
			    $this->bind(":xml_path", $xmlPath);
			    $this->bind(":author", $author);
				  $this->bind(":book_name", $name);
					$recordDb = $this->fetchOne();

			    if ($recordDb) {
			    	$this->query('UPDATE books SET record_date = current_timestamp WHERE id = :id');
			    	$this->bind(":id", $recordDb['id']);
			    	$this->execute();
			    } else {
			    
				    $this->query('INSERT INTO books (author, book_name, record_date, xml_path) VALUES (:author, :book_name, current_timestamp, :xml_path)');
				    $this->bind(":author", $author);
				    $this->bind(":book_name", $name);
				    $this->bind(":xml_path", $xmlPath);
						$this->execute();

						$lastId = $this->lastInsertId();

						if(!empty($lastId)){
					  	$this->query('SELECT * FROM books WHERE id = :lastId');
					  	$this->bind(":lastId", $lastId);
							$insertedRecord = $this->fetchOne();
							array_push($rows, $insertedRecord);
						}

						Messages::setMessage('Records Inserted', 'success');
					}
			  }
			}
			return $rows;

		}
	}

	public function search() {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$this->query('SELECT * FROM books WHERE author = :author');
			$this->bind(":author", $_POST['author']);
			$rows = $this->fetch();
			return $rows;
		}
	}

	public function dropdown() {
		$this->query('SELECT author FROM books');
		$rows = $this->fetch();
		$uni = array_unique($rows, SORT_REGULAR);
		
		echo "<option style='display:none'></option>";
		foreach ($uni as $authorU) {
			foreach ($authorU as $key => $value) {
				echo "<option value='$value'>$value</option>";
			}
		}
	}

	
}